FROM debian:11-slim

ENV DEBIAN_FRONTEND=noninteractive

RUN apt update -qy && \
    apt upgrade -qy && \
    apt install -qy \
    vim \
    inetutils-ping \
    inetutils-traceroute \
    inetutils-telnet \
    inetutils-ftp \
    tcpdump \
    tcpreplay \
    openssh-client \
    # dig, nslookup
    bind9-dnsutils \
    # netstat, route, arp
    net-tools \
    iproute2 \
    arping \
    gpg \
    bash \
    curl \
    wget \
    mariadb-client \
    redis-tools \
    jq \
    netcat-openbsd \
    lsb-release \
    # watch, pkill, vmstat, top
    procps \
    unzip && \
    # Install aws cli v2
    curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
    unzip awscliv2.zip && \
    ./aws/install && \
    rm awscliv2.zip && \
    rm -rf ./aws && \
    # Install consul
    wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor -o /usr/share/keyrings/hashicorp-archive-keyring.gpg && \
    echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | tee /etc/apt/sources.list.d/hashicorp.list && \
    apt update -qy && \
    apt install consul && \
    apt-get clean -y && \
    apt-get autoremove -y && \
    rm -rf /tmp/* && \
    rm -rf /var/lib/apt/lists/*

ENTRYPOINT ["/bin/bash"]


