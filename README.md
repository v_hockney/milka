# Milka

A debian container with useful tools for debugging and connecting to network applications.

## Usage

Currently available via the gitlab container registry at https://gitlab.com/v_hockney/milka/container_registry.

## Included software

- vim
- ping
- traceroute
- telnet
- openssh-client (https://packages.debian.org/bullseye/openssh-client)
  - ssh
  - scp
  - sftp
  - ...
- ftp
- tcpdump
- tcpreplay (https://packages.debian.org/bullseye/tcpreplay)
- net-tools (https://packages.debian.org/bullseye/net-tools)
  - netstat
  - route
  - arp
  - ...
- bind9-dnsutils (https://packages.debian.org/bullseye/bind9-dnsutils)
  - dig
  - nslookup
  - nsupdate
- iproute2 (https://packages.debian.org/bullseye/iproute2)
- arping
- bash
- curl
- wget
- mariadb-client
- redis-tools
- jq
- netcat (openbsd)
- procps
  - watch
  - pkill
  - ps
  - pgrep
  - top
  - vmtop
  - ...
- gpg
- lsb-release
- awscliv2
- consul

